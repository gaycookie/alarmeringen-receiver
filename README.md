# Alarmeringen Backend

### Docker Compose
Because I've included a Docker Compose file with the project,
If you run it the first time, it will build the image first.  
If you decide to change something and you need to rebuild it again use `--build`.

```bash
docker-compose up -d
```

### Environment Variables
- **TZ**: The timezone for the container, very important if you do not want to use UTC (default timezone).
- **MYSQL_HOSTNAME**: The host/hostname where your MySQL server is running.
- **MYSQL_USERNAME**: The username you want to use to connect to your MySQL server.
- **MYSQL_PASSWORD**: The password that belongs to the username of your MySQL server.
- **MYSQL_DATABASE**: The database you want the messages to be stored in.