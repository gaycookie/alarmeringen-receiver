FROM ubuntu:22.04

WORKDIR /bin/alarmeringen
COPY . .

RUN mkdir -p /etc/apt/keyrings
RUN apt-get update && apt-get install -y ca-certificates curl gnupg librtlsdr-dev rtl-sdr multimon-ng
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update && apt-get install -y nodejs

RUN npm install -g npm@latest
RUN npm install knex mysql2

ENV TZ=Europe/Amsterdam
ENV MYSQL_HOSTNAME=localhost
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=SuperSecret
ENV MYSQL_DATABASE=alarmeringen

RUN echo 'blacklist dvb_usb_rtl28xxu' | tee --append /etc/modprobe.d/blacklist-dvb_usb_rtl28xxu.conf
CMD ["/bin/sh", "-c", "rtl_fm -f 169.65M -M fm -s 22050 -p 83 -g 30 | multimon-ng -q -a FLEX -t raw - | npm start" ]