#!/usr/bin/env node

const knex = require('knex')({
  client: 'mysql2',
  connection: {
    host: process.env.MYSQL_HOSTNAME,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  }
});

const stdin = process.openStdin();

knex.schema.hasTable('reports').then(exists => {
  if (!exists) {
    return knex.schema.createTable('reports', table => {
      table.increments('id').primary();
      table.datetime('datetime').notNullable();
      table.text('message').notNullable();
    });
  }
});

knex.schema.hasTable('capcodes').then(exists => {
  if (!exists) {
    return knex.schema.createTable('capcodes', table => {
      table.increments('id').primary();
      table.string('capcode', 32).notNullable().unique();
      table.text('capcode_name');
    });
  }
});

knex.schema.hasTable('report_capcode').then(exists => {
  if (!exists) {
    return knex.schema.createTable('report_capcode', table => {
      table.increments('id').primary();
      table.integer('report_id').unsigned().notNullable();
      table.integer('capcode_id').unsigned().notNullable();
      table.foreign('report_id').references('id').inTable('reports');
      table.foreign('capcode_id').references('id').inTable('capcodes');
    });
  }
});

stdin.on('data', async data => {
  const report = data.toString().trim();

  console.log(report); // Just for testing here, will be removed some day xD
  const regex = /^FLEX\|(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\|[^|]+\|[^|]+\|([^|]+)\|[^|]+\|(.+$)/
  // Totally not sure if this is a better approach, but the previous approach gave issues
  
  const match = report.match(regex);
  if (!match) return;

  const datetime = match[1];
  const capcodes = match[2].split(' ');
  const message = match[3];

  knex.transaction(async trx => {
    const [reportId] = await trx('reports').insert({ datetime, message });
  
    for (const singleCapcode of capcodes) {
      const [insertCapcodeId] = await trx('capcodes').insert({ capcode: singleCapcode }).onConflict('capcode').ignore();

      let capcodeId = insertCapcodeId;
      if (!capcodeId) {
        const existingCapcodeId = await trx('capcodes').select('id').where({ capcode: singleCapcode }).first();
        capcodeId = existingCapcodeId.id;
      }
      
      await trx('report_capcode').insert({ report_id: reportId, capcode_id: capcodeId });
    }
  }).catch((err) => {
    console.error('Error inserting report and capcodes:', err);
    console.log({ datetime, capcodes, message });
  });
});